package com.example.demo.controllers;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AddAboutController {
    @GetMapping("/showAbout")
    public String index(){
        return "about";
    }
}