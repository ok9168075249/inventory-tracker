INSERT INTO PARTS Values( 1,111,10,30,15,'Central Processing Unit', 400.0,1,null);
INSERT INTO PARTS Values( 1,222,15,50,15,'Graphics Processing Unit',300.0,2,null);
INSERT INTO PARTS Values( 1,333,20,40,10,'Random Access Memory',    100.0,3,null);
INSERT INTO PARTS Values( 2,444,30,60,15,'Power Supply Unit',       100.0,null,'PSU Company');
INSERT INTO PARTS Values( 2,555,25,40,10,'Motherboard',             200.0,null,'Motherboard Company');

INSERT INTO PRODUCTS Values(1,5,  'Enterprise Computer',    1500.0);
INSERT INTO PRODUCTS Values(2,10, 'Gaming Computer',        1500.0);
INSERT INTO PRODUCTS Values(3,15, 'College Computer',       1750.0);
INSERT INTO PRODUCTS Values(4,20, 'Graphic Design Computer',2000.0);
INSERT INTO PRODUCTS Values(5,25, 'Personal Computer',      1000.0);

INSERT INTO PRODUCT_PART Values(111,1);
INSERT INTO PRODUCT_PART Values(222,2);
INSERT INTO PRODUCT_PART Values(333,3);
INSERT INTO PRODUCT_PART Values(444,4);
INSERT INTO PRODUCT_PART Values(555,5);